# (c) 2018 by Rhonda D'Vine <rhonda@debian.org>


use strict;
#use Irssi 20020428; # for Irssi::signal_continue
use Irssi; # usually we would use the above - but there is no "auto/Irssi/version.al" file
use LWP::UserAgent;
use HTML::Entities qw(decode_entities);
use JSON qw(from_json);
use XML::LibXML::Simple qw(XMLin);
use Date::Manip qw(Date_SecsSince1970 Date_SecsSince1970GMT);
use POSIX qw(strftime);
use utf8::all;

# stores general data
my $dcschedule;
$dcschedule->{'rev'}  = '3.0.0';
$dcschedule->{'date'} = '2019-10-24';

use vars qw($VERSION %IRSSI);

$VERSION = $dcschedule->{'rev'};

# This script requires a few things:
# The cron script loaded
# /set dcschedule_attendees_auth Super Secret Auth String
#

%IRSSI = (
	'authors'     => "Rhonda D'Vine",
	'contact'     => 'rhonda@debian.org',
	'name'        => 'dcschedule',
	'description' => 'Info Bot for Debconf Channels',
	'licence'     => 'BSD',
	'url'         => 'https://salsa.debian.org/rhonda/schedulebot',
	'changed'     => $dcschedule->{'date'},
);

my $irssidir = Irssi::get_irssi_dir();


$dcschedule->{TZ}        = 'Asia/Kolkata';
#$dcschedule->{mealplace} = 'Pizzeria Antalya';
$dcschedule->{mealplace} = 'Meeshapulimala';
$dcschedule->{mealnow}   = 0;
$dcschedule->{room}->{'Anamudi'}  = '#debconf-anamudi';
$dcschedule->{room}->{'Kuthiran'} = '#debconf-kuthiran';
$dcschedule->{room}->{'Ponmudi'}  = '#debconf-ponmudi';

# some rather static informations we use - we should be able to get them through some vars ...
$dcschedule->{'myMasters'} = {
	## WHO CAN CONTROL US ## {{{
	########################
	'Rhonda@127.0.0.1' => {
		'nick'   => 'Rhonda',
		'attrib' => "m'Lady"
	},
	'~rhonda@0000f83c.user.oftc.net' => {
		'nick'   => 'Rhonda',
		'attrib' => "m'Lady"
	},
	'~nattie@shell.jonathancarter.org' => {
		'nick'   => 'nattie',
		'attrib' => "Mistress"
	},
	'~quassel@2a05:d018:c1e:8d00:c473:4128:ae2d:2c36' => {
		'nick'   => 'srud',
		'attrib' => "Dearie"
	},
	'~gwolf@mosca.iiec.unam.mx' => {
		'nick'   => 'gwolf',
		'attrib' => "Sir"
	},
	'~gwolf@mosca.iiec.unam.mx' => {
		'nick'   => 'gwolf',
		'attrib' => "Sir"
	},
	'~terceiro@000126b0.user.oftc.net' => {
		'nick'   => 'terceiro',
		'attrib' => "Sir"
	},
	'~mbanck@ppp-188-174-61-10.dynamic.mnet-online.de' => {
		'nick'   => 'azeem',
		'attrib' => "Dearie"
	},
	'~mbanck@ppp-93-104-191-173.dynamic.mnet-online.de' => {
		'nick'   => 'azeem',
		'attrib' => "Dearie"
	},
	'~olasd@noether.olasd.eu' => {
		'nick'   => 'olasd',
		'attrib' => "Dearie"
	},
};	# }}}



# Maintainer & original Author:  Rhonda D'Vine <rhonda@debian.org>

# Version-History:
# ================
# 3.0.0 -- update for privacyweek 2019, add toot support through chaos.social
# 2.0.0 -- update for dc19
# 1.0.0 -- get_food rudimentarily done
# 0.7.0 -- get_bar done
# 0.6.0 -- also announce in corresponding chatroom
# 0.5.0 -- arrived status overhaul
# 0.4.2 -- Actually talk in #debconf, not #debconf-testing
# 0.4.1 -- make it possible to get loaded when the auth string isn't correct/set yet
# 0.4.0 -- meal announce
# 0.3.0 -- get_mao, seen, arrived, polygen
# 0.2.1 -- time fixes
# 0.2.0 -- schedule done
# 0.1.0 -- first scratch, arrivals done

# TODO List
# [ ] hardcoded timezone staff (e.g. in get_bar)
# [ ] lots of hardcoded stuff, like URLs to fetch
# [ ] proper get_food function needed, with expected time




sub cmd_reload_arrivals () {
	## check ARRIVALS ## {{{
	####################
	my %arrived;
	my $server = Irssi::server_find_tag('OFTC');

	# fetch the data - TODO: error handling
	my $ua = LWP::UserAgent->new(
		'agent' => "dcschedule $VERSION"
	);
	my $response = $ua->get('https://debconf23.debconf.org/register/attendees/admin/export/arrived/',
		Authorization => Irssi::settings_get_str('dcschedule_attendees_auth'));

	if (not $response->is_success) {
		# error while fetching
		Irssi::print("There was some issue with fetching the attendee list: " . $response->status_line);
		return;
	}

	my $json = $response->decoded_content;
	$json = from_json($json);

	foreach my $user (@{$json->{arrived}}) {
		$user->{arrived}  = JSON->boolean($user->{arrived})  ? 1 : 0;
		$user->{departed} = JSON->boolean($user->{departed}) ? 1 : 0;

		# this pollutes ->{name}, ->{username} and ->{nick}
		$arrived{$user->{username}} = $user;
	}

	# Pollito is always here
	$arrived{'-1'}->{name} = "Caucho Gallus-Gallus";
	$arrived{'-1'}->{nick} = "Pollito";
	$arrived{'-1'}->{arrived} = 1;

	use Data::Dumper;
	open DUMP, ">/tmp/rhonda-arrived.dump";
	print DUMP Dumper(\%arrived);


	# add them into the current attendee data
	for my $attendee (keys %arrived) {
		if (not exists $dcschedule->{arrived}->{$attendee} || not exists $dcschedule->{arrived}->{$attendee}->{arrived}
		    || $dcschedule->{arrived}->{$attendee}->{arrived} != $arrived{$attendee}->{arrived}) {
			#$dcschedule->{arrived}->{$attendee} = $arrived{$attendee};
			$dcschedule->{arrived}->{$attendee}->{username} = $attendee;
			$dcschedule->{arrived}->{$attendee}->{name}     = $arrived{$attendee}->{name};
			$dcschedule->{arrived}->{$attendee}->{nick}     = $arrived{$attendee}->{nick};
			$dcschedule->{arrived}->{$attendee}->{arrived}  = $arrived{$attendee}->{arrived};
			$dcschedule->{arrived}->{$attendee}->{departed} = $arrived{$attendee}->{departed};
		} else {
			delete $arrived{$attendee};
		}
	}

	print DUMP Dumper($dcschedule->{arrived});
	close DUMP;



	# there have people arrived
	if (keys %arrived) {
		my $msg = "New arrivals: ";
		for my $attendee (keys %arrived) {
			if ($arrived{$attendee}->{name} eq "Joerg Jaspert") {
				$server->command("msg #debconf Ladies and Gentlemen, your attention please:");
				$server->command("msg #debconf Ganneff has entered the building!!!");
				delete $arrived{$attendee};
				next;
			}
			$msg .= $arrived{$attendee}->{name} . ", ";
		}
		if (length($msg) > 170) {
			Irssi::print("Lots of people (" . scalar (keys %arrived) . ") arrived.  I can't be bothered to announce them all");
			return;
		}
		$msg =~ s/,\s+$//;
		#$server->command("msg #debconf $msg");
	}

	# this is debug print only
	for my $attendee (keys %arrived) {
		Irssi::print($attendee . "[new]");
	}
} ## }}}

sub cmd_reload_schedule () {
	## reload SCHEDULE ## {{{
	#####################
	my $server = Irssi::server_find_tag('OFTC');

	# fetch the data - TODO: error handling
	my $ua = LWP::UserAgent->new(
		'agent' => "dcschedule $VERSION"
	);
	my $response = $ua->get('https://debconf23.debconf.org/schedule/pentabarf.xml');
	my $xml = $response->decoded_content;

	my $data = XMLin($xml, ForceArray => 1,
	);

	foreach my $day (0 .. $#{$data->{day}}) {
		foreach my $room (keys %{$data->{day}[$day]->{room}}) {
			# only look into rooms if there are events for it
			if (defined $data->{day}[$day]->{room}->{$room}->{event}) {
				foreach my $event (keys %{$data->{day}[$day]->{room}->{$room}->{event}}) {
					next if $event eq '';

					# grab the speakers
					my @speakers;
					if ($data->{day}[$day]->{room}->{$room}->{event}->{$event}->{persons} ne "\n") {

						foreach my $person (keys %{$data->{day}[$day]->{room}->{$room}->{event}->{$event}->{persons}[0]->{person}}) {
							push @speakers, $data->{day}[$day]->{room}->{$room}->{event}->{$event}->{persons}[0]->{person}->{$person}->{content};
						}
					}

					my $speakers = ($#speakers >= 0) ? join (', ', sort @speakers) : undef;

					# the date variable is given in local time
					my ($evyr, $evmon, $evday, $evhr, $evmin, $evsec, $evoffhr, $effofmin) = $data->{day}[$day]->{room}->{$room}->{event}->{$event}->{date} =~ /(\d+)-(\d+)-(\d+)T(\d+):(\d+):(\d+)([-+]\d+):(\d+)/;
					#my $evstart = Date_SecsSince1970GMT($evmon,$evday,$evyr,$evhr,$evmin,0) + $evoffhr * 3600 + $effofmin * 60;
					my $evstart = Date_SecsSince1970GMT($evmon,$evday,$evyr,$evhr,$evmin,0) + 0 * 3600 + 0 * 60; # TODO

					my ($durhr, $durmin) = $data->{day}[$day]->{room}->{$room}->{event}->{$event}->{duration} =~ /(\d+):(\d+)/;

					my $evend = $evstart + $durhr * 60*60 + $durmin * 60;
					my (undef,$enmin,$enhr) = localtime($evend);

					# the corresponding ID for the video stream URL
					my %room_video_id = (
						"Talks"   => 1,
						);

					# store the current mealplace
					#if ($data->{day}[$day]->{room}->{$room}->{event}->{$event}->{url} =~ m,/meals/,) {
					#mealplace not stored in the schedule
					if ($data->{day}[$day]->{room}->{$room}->{event}->{$event}->{url} =~ m,/(breakfast|lunch|barbecue|afternoon-break|dinner)/,) {
						$data->{day}[$day]->{room}->{$room}->{event}->{$event}->{room} = $dcschedule->{mealplace};
					}

					for my $announce (30, 15, 5, 0) {
					#for my $announce (60, 59, 58, 57, 56, 55, 54, 53, 52, 51, 50, 49, 48, 47, 46, 45, 44, 43, 42, 41, 40, 39, 38, 37, 36, 35, 34, 33, 32, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0) {
						# process data, put them into structure
						$dcschedule->{events}->{$evstart-$announce*60}->{$event}->{title}    = $data->{day}[$day]->{room}->{$room}->{event}->{$event}->{title};
						$dcschedule->{events}->{$evstart-$announce*60}->{$event}->{type}     = $data->{day}[$day]->{room}->{$room}->{event}->{$event}->{type};
						$dcschedule->{events}->{$evstart-$announce*60}->{$event}->{authors}  = $speakers;
						($dcschedule->{events}->{$evstart-$announce*60}->{$event}->{date})   = $data->{day}[$day]->{room}->{$room}->{event}->{$event}->{date} =~ /(\d+-\d+-\d+)T/;
						$dcschedule->{events}->{$evstart-$announce*60}->{$event}->{start}    = sprintf "%02d:%02d", $evhr, $evmin;
						$dcschedule->{events}->{$evstart-$announce*60}->{$event}->{end}      = sprintf "%02d:%02d", $enhr, $enmin;
						$dcschedule->{events}->{$evstart-$announce*60}->{$event}->{room}     = $data->{day}[$day]->{room}->{$room}->{event}->{$event}->{room};
						$dcschedule->{events}->{$evstart-$announce*60}->{$event}->{room_id}  = $room_video_id{$data->{day}[$day]->{room}->{$room}->{event}->{$event}->{room}};
						$dcschedule->{events}->{$evstart-$announce*60}->{$event}->{released} = $data->{day}[$day]->{room}->{$room}->{event}->{$event}->{released};
						$dcschedule->{events}->{$evstart-$announce*60}->{$event}->{url} = $data->{day}[$day]->{room}->{$room}->{event}->{$event}->{url};
						$dcschedule->{events}->{$evstart-$announce*60}->{$event}->{status}   = "will start in $announce minutes";
					}
					$dcschedule->{events}->{$evstart}->{$event}->{status}   = "has just begun";

					#if ($data->{day}[$day]->{room}->{$room}->{event}->{$event}->{url} =~ m,/meals/,) {
					if ($data->{day}[$day]->{room}->{$room}->{event}->{$event}->{url} =~ m,/(breakfast|lunch|barbecue|afternoon-break|dinner)/,) {
						for my $announce (30, 15, 5, 0) {
						#for my $announce (60, 59, 58, 57, 56, 55, 54, 53, 52, 51, 50, 49, 48, 47, 46, 45, 44, 43, 42, 41, 40, 39, 38, 37, 36, 35, 34, 33, 32, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0) {
							# process data, put them into structure
							$dcschedule->{events}->{$evend-$announce*60}->{$event}->{title}    = $data->{day}[$day]->{room}->{$room}->{event}->{$event}->{title};
							$dcschedule->{events}->{$evend-$announce*60}->{$event}->{type}     = $data->{day}[$day]->{room}->{$room}->{event}->{$event}->{type};
							$dcschedule->{events}->{$evend-$announce*60}->{$event}->{authors}  = $speakers;
							($dcschedule->{events}->{$evend-$announce*60}->{$event}->{date})   = $data->{day}[$day]->{room}->{$room}->{event}->{$event}->{date} =~ /(\d+-\d+-\d+)T/;
							$dcschedule->{events}->{$evend-$announce*60}->{$event}->{start}    = sprintf "%02d:%02d", $evhr, $evmin;
							$dcschedule->{events}->{$evend-$announce*60}->{$event}->{end}      = sprintf "%02d:%02d", $enhr, $enmin;
							$dcschedule->{events}->{$evend-$announce*60}->{$event}->{room}     = $data->{day}[$day]->{room}->{$room}->{event}->{$event}->{room};
							$dcschedule->{events}->{$evend-$announce*60}->{$event}->{room_id}  = $room_video_id{$data->{day}[$day]->{room}->{$room}->{event}->{$event}->{room}};
							$dcschedule->{events}->{$evend-$announce*60}->{$event}->{released} = $data->{day}[$day]->{room}->{$room}->{event}->{$event}->{released};
							$dcschedule->{events}->{$evend-$announce*60}->{$event}->{url} = $data->{day}[$day]->{room}->{$room}->{event}->{$event}->{url};
							$dcschedule->{events}->{$evend-$announce*60}->{$event}->{status}   = "will end in $announce minutes";
						}
						$dcschedule->{events}->{$evend}->{$event}->{status}   = "has just finished";
					}
				}
			}
		}
	}

	use Data::Dumper;
	open DUMP, ">/tmp/schedule.dump";
	print DUMP Dumper(\$data);
	print DUMP Dumper($dcschedule->{events});
	close DUMP;

} ## }}}

sub cmd_periodic_work () {
	## announce stuff ## {{{
	####################
	my $server = Irssi::server_find_tag('OFTC');

	my $ua = LWP::UserAgent->new(
		'agent' => "dcschedule $VERSION"
	);
	$ua->default_header("Authorization" => Irssi::settings_get_str('dcschedule_tootbot_auth'),
			    "Idempotency-Key" => int rand 100000);
	my (undef, $min, $hr, $day, $mon, $yr) = localtime(time);
	my $now = strftime("%s", 0, $min, $hr, $day, $mon, $yr);

	if (exists $dcschedule->{events}->{$now}) {
		foreach my $id (keys %{$dcschedule->{events}->{$now}}) {
			my $msg = join (' ',
				$dcschedule->{events}->{$now}->{$id}->{title},
				(length($dcschedule->{events}->{$now}->{$id}->{authors}) ? "by $dcschedule->{events}->{$now}->{$id}->{authors}" : ()),
				"in $dcschedule->{events}->{$now}->{$id}->{room}",
				$dcschedule->{events}->{$now}->{$id}->{status},
                                ((defined $dcschedule->{events}->{$now}->{$id}->{url} && $dcschedule->{events}->{$now}->{$id}->{url} !~ m,/(breakfast|lunch|barbecue|afternoon-break|dinner)/,) ? $dcschedule->{events}->{$now}->{$id}->{url} : ()),
			);
			$server->command("msg #debconf $msg");

			# is it food start/end?
			if ($dcschedule->{events}->{$now}->{$id}->{url} =~ m,/(breakfast|lunch|barbecue|afternoon-break|dinner)/,) {
				if ($dcschedule->{events}->{$now}->{$id}->{status} eq "has just finished") {
					$dcschedule->{mealnow}   = 0;
				} elsif ($dcschedule->{events}->{$now}->{$id}->{status} eq "has just begun") {
					$dcschedule->{mealnow}   = 1;
				}
			}

			# also announce it in the corresponding channel for the
			# room
			my $room = $dcschedule->{events}->{$now}->{$id}->{room};
			if (defined $dcschedule->{room}->{"$room"}) {
				$server->command("msg " . $dcschedule->{room}->{"$room"} . " $msg");
			}

			# if it is beginning now, toot it
			if (($dcschedule->{events}->{$now}->{$id}->{status} eq "has just begun") && !($dcschedule->{events}->{$now}->{$id}->{url} =~ m,/(breakfast|lunch|barbecue|afternoon-break|dinner|about)/,)) {
				my $status = join (' ',
					'"' . $dcschedule->{events}->{$now}->{$id}->{title} . '"',
					(length($dcschedule->{events}->{$now}->{$id}->{authors}) ? "by $dcschedule->{events}->{$now}->{$id}->{authors}" : ()),
					"in $dcschedule->{events}->{$now}->{$id}->{room}",
					((defined $dcschedule->{events}->{$now}->{$id}->{url} && $dcschedule->{events}->{$now}->{$id}->{url} !~ m,/(breakfast|lunch|barbecue|afternoon-break|dinner)/,) ? $dcschedule->{events}->{$now}->{$id}->{url} : ()),
				);
				$ua->default_header("Authorization" => Irssi::settings_get_str('dcschedule_tootbot_auth'),
						    "Idempotency-Key" => int rand 100000);
				$ua->post("https://chaos.social/api/v1/statuses",
					[
						# https://docs.joinmastodon.org/methods/statuses
						status => $status,
						visibility => "unlisted"
					]);
				use Data::Dumper;
				open DUMP, ">/tmp/tatus.dump";
				print DUMP Dumper($status, Irssi::settings_get_str('dcschedule_tootbot_auth'));
			}

			sleep 1;
		}
	}

	&cmd_reload_arrivals;
} ## }}}


sub are_they_here ($$$$) {
	## ARE THEY HERE ## {{{
	###################
	my ($server, $target, $nick, $person) = @_;

	# create array of matches
	my @names;
	use Data::Dumper;
	open DUMP, ">/tmp/arrived.dump";
	print DUMP Dumper($person, $dcschedule->{arrived});
	close DUMP;
	for my $attendee (keys %{ $dcschedule->{arrived} }) {
		# these information are stored in the json now too :)
		next unless $dcschedule->{arrived}->{$attendee}->{arrived} && ! $dcschedule->{arrived}->{$attendee}->{departed};

		if ($dcschedule->{arrived}->{$attendee}->{name} =~ m/$person/i ||
			$dcschedule->{arrived}->{$attendee}->{nick} =~ m/$person/i ||
			$dcschedule->{arrived}->{$attendee}->{username} =~ m/$person/i) {

			push @names, $dcschedule->{arrived}->{$attendee}->{name};
		}
	}

	# check how many matches there are
	my $msg;
	if ($#names >= 1) {
		# more than one match
		$msg = sprintf "%s: They might be here. I've seen people named %s",
			$nick, join ", ", @names;
	} elsif ($#names == 0) {
		# one match
		$msg = sprintf "%s: Yes, %s is here",
			$nick, $names[0];
	} else {
		# no match
		$msg = sprintf "%s: No, I haven't heard of %s",
			$nick, $person;
	}

	# don't spit out long messages
	if (length($msg) > 150) {
		$msg = sprintf "%s: maybe not, but I can't be bothered to reply to such a vague query. Ask me something more specific, damnit.",
			$nick;
	}

	$server->command("msg $target $msg");
} ## }}}

sub get_bar ($$$) {
	## GET BAR ## {{{
	#############
	my ($server, $target, $nick) = @_;

	# TODO: still timezone related hardcoded stuff
	my (undef, $min, $hr, $day, $mon, $yr) = localtime(time);
	my $now = (strftime("%s", 0, $min, $hr, $day, $mon, $yr) - 0 * 3600) % 86400;

	my $msg;
	if ( ($now >= 14 * 3600) || ($now <= 1*3600)) {
		# open
		$msg = "The bar is open, hurry up!";

	} elsif ( ($now < 14 * 3600) && ($now >= 12 * 3600) ) {
		$msg = sprintf ("The bar will open in aroundish %d hours and %d minutes, don't go by the clock though…",
			(20 * 3600 - $now) / 3600,
			((20 * 3600 - $now) % 3600) / 60);
	} else {
		$msg = sprintf ("The bar closed %d hours and %d minutes ago, or earlier if you people were lazy…",
			($now + 1 * 3600) / 3600,
			(($now + 1 * 3600) % 3600) / 60);
	}

	Irssi::print($now);

	$server->command("msg $target $nick: $msg");
} ## }}}



sub handle_command ($$$$$) {
	## GENERAL SIGNAL HANDLER ## {{{
	############################
	my ($server, $target, $nick, $address, $command) = @_;

	if ($command =~ /^reload$/ or $command =~ /^reconnect$/) {
		# master accounts - check if master calls us
		if ($dcschedule->{'myMasters'}->{$address}) {
			if ($command =~ /^reload$/) {
				## RELOAD COMMAND ## {{{
				####################
				my $check = qx(perl -c $irssidir/scripts/$IRSSI{'name'}.pl 2>&1);
				if ($check =~ /syntax OK/) {
					$server->command("msg $target $nick: Yes, $dcschedule->{'myMasters'}->{$address}->{'attrib'}!");
					# reload arrivals before reloading the
					# script, that information might get
					# lost otherwise
					&cmd_reload_arrivals;
					Irssi::command("script load $IRSSI{'name'}");
				} else {
					$server->command("msg $target $nick: $dcschedule->{'myMasters'}->{$address}->{'attrib'}, someone corrupted me, don't dare to...");
					$server->window_item_find($target)->print("[dcschedule] $check", Irssi::MSGLEVEL_CRAP);
				}
				## }}}

			} elsif ($command =~ /^reconnect$/) {
				## RECONNECT COMMAND ## {{{
				#######################
				$server->window_item_find($target)->print("$address asks for reconnect ... @_", Irssi::MSGLEVEL_CRAP);
				#$server->command("msg $target $nick: Yes, $dcschedule->{'myMasters'}->{$address}->{'attrib'}!");
				# Reconnect->{'tag' => $server->{'tag'}};
				#$server->reconnect("$dcschedule->{'myMasters'}->{$address}->{'attrib'} asked for it ...");
				# --FIXME-- segfaults in notify_nick_find, no idea currently why.
				$server->command("msg $target $nick: I'm a coward, $dcschedule->{'myMasters'}->{$address}->{'attrib'}, reconnect would let me die!");
				## }}}
			}
		} else {
			$server->command("msg $target $nick: Who are you to use that tone on me??");
		}

	} elsif ($command =~ /^liar/i) {
		## LIAR ## {{{
		##########
		$server->command("msg $target I'm a liar! LIAR! I tell you, don't believe me! I'm a liar and you are soooo smart playing with me!");
		## }}}

	} elsif ($command =~ /^where is /i) {
		## WHERE IS ## {{{
		##############
		$server->command("msg $target $nick: How should I know? Do I look like some big brother, or what?");
		## }}}

	} elsif ($command =~ / seen? /i) {
		## SEE ## {{{
		#########
		$server->command("msg $target $nick: Sorry, no clue, I am only a poor bot with no eyes, can't see anything.");
		## }}}

	} elsif ($command =~ /^(have I arrived|am I here)/i) {
		## SELF ARRIVED ## {{{
		##################
		$server->command("msg $target $nick: If you don't know yourself, I'm not making up your mind…");
		## }}}

	} elsif ($command =~ /^(have you arrived|are you here)/i) {
		## YOU ARRIVED ## {{{
		#################
		$server->command("msg $target $nick: Question is, have you arrived…");
		## }}}

	} elsif ($command =~ /^mao/i) {
		## MAO ## {{{
		#########
		my @msg = `/usr/games/polygen /usr/share/polygen/eng/debian/mao.grm`;

		my $text;
		if (scalar @msg > 4) {
			$text = sprintf ("%s: Not for you!", $nick);
		} else {
			$text = join '', @msg;
		}

		$server->command("msg $target $text");
		## }}}

	} elsif ($command =~ /^(?:has|did|is) (.*) (?:arrived(?: yet)?|here)?/i) {
		## HAS ARRIVED ## {{{
		#################
		&are_they_here($server, $target, $nick, $1);
		## }}}

	} elsif ($command =~ /^(?:I'm thirsty|beer|drink|milk)/i) {
		## GET BAR ## {{{
		#############
		&get_bar($server, $target, $nick);
		## }}}

	} elsif ($command =~ /^(?:food)/i) {
		## GET FOOD ## {{{
		##############

		if ($dcschedule->{mealnow}) {
			$server->command("msg $target $nick: $dcschedule->{mealplace} is serving food, hurry up!");
		} else {
			$server->command("msg $target $nick: we are currently out of meal times, please don't starve…");
		}
		## }}}

	} elsif ($command =~ /^hug ([^\s]+)/i) {
		## HUG SOMEONE ## {{{
		#################
		my $huggie = $1;
		if ($huggie eq $server->{'nick'}) {
			$server->command("action $target hugs itself, which looks pretty strange...");
		} else {
			$server->command("action $target hugs $huggie");
		}
		## }}}

	} elsif ($command =~ /^kick ([^\s]+)/i) {
		## KICK SOMEONE ## {{{
		##################
		my $kick = $1;
		$server->command("action $target kicks $nick instead, do your dirty work yourself.");
		## }}}

	} elsif ($command =~ /^help$/i) {
		## HELP ## {{{
		##########
		$server->command("msg $target $nick: I could tell you about \002stuff\002... you name it.");
		## }}}

	} elsif ($command =~ /^weather/i) {
		## WEATHER ## {{{
		#############
		$server->command("msg $target $nick: If it's not already raining it will be soon.  Don't forget your umbrella!");
		## }}}

	} else {
		## NOTHING ## {{{
		#############
		opendir(DIR, "/usr/share/polygen/eng/debian/");
		my @files = grep(/\.grm$/,readdir(DIR));
		closedir(DIR);

		# blacklist some really annoying grammar
		@files = grep (!/^(complaints|gr|dplquestion)\.grm/, @files);

		my $grammar = $files[rand scalar @files];
		my @msg = `/usr/games/polygen "/usr/share/polygen/eng/debian/$grammar"`;

		my $text;
		if (scalar @msg > 4) {
			$text = sprintf ("%s: Not for you!", $nick);
		} else {
			$text = join '', @msg;
		}

		$server->command("msg $target $text");
		## }}}
	}
} ## }}}


sub sig_public {
	## HANDLE PUBLIC SIGNALS ## {{{
	###########################
	my ($server, $msg, $nick, $address, $channel) = @_;
	# first, continue with the outter signal
	Irssi::signal_continue(@_);

	my $mynick = $server->{'nick'};
	if ($msg =~ /^$mynick[:, ]\s*(.*\S)/) {
		$msg = $1;
		&handle_command($server, $channel, $nick, $address, $msg);
	}
} ## }}}

sub sig_private {
	## HANDLE PRIVATE SIGNALS ## {{{
	############################
	my ($server, $msg, $nick, $address) = @_;
	# first, continue with the outter signal
	Irssi::signal_continue(@_);

	&handle_command($server, $nick, $nick, $address, $msg);
} ## }}}


Irssi::command_bind('reload_arrivals', 'cmd_reload_arrivals');
Irssi::command_bind('reload_schedule', 'cmd_reload_schedule');

Irssi::command_bind('periodic_work', 'cmd_periodic_work');

Irssi::settings_add_str('dcschedule', 'dcschedule_attendees_auth', '');
Irssi::settings_add_str('dcschedule', 'dcschedule_tootbot_auth', '');

Irssi::signal_add_last('message public',  'sig_public');
Irssi::signal_add_last('message private', 'sig_private');

&cmd_reload_arrivals;
&cmd_reload_schedule;
Irssi::command("script exec \$ENV{TZ} = $dcschedule->{TZ}");

# vim:fdm=marker:nowrap:
